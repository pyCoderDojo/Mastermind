import pygame, sys
from pygame.locals import *

pygame.init()
fpsClock = pygame.time.Clock()

(boardsize) = (320, 400)
window = pygame.display.set_mode(boardsize)
blue = (0, 0, 205)
red = (205, 0, 0)
green = (0, 205, 0)
white = (205, 205, 205)


def initRows(rc, r):
    dbs = [blue, red, green, white]
    tbs = [white, white, white, white]

    r[str(rc)] = (dbs, tbs)

def drawit(rc, r):

    yr = 10 + int(rc) * 30
    (dbs, tbs) = r

    # display balls
    pygame.draw.rect(window, dbs[0], (10, yr + 10, 10, 10))
    pygame.draw.rect(window, dbs[1], (20, yr + 10, 10, 10))
    pygame.draw.rect(window, dbs[2], (10, yr + 20, 10, 10))
    pygame.draw.rect(window, dbs[3], (20, yr + 20, 10, 10))
    # test balls
    pygame.draw.circle(window, tbs[0], (50, yr + 20), 10)
    pygame.draw.circle(window, tbs[1], (80, yr + 20), 10)
    pygame.draw.circle(window, tbs[2], (110, yr + 20), 10)
    pygame.draw.circle(window, tbs[3], (140, yr + 20), 10)


rows = {}
for i in range(0,11):
    initRows(i, rows)

while True:

    (dbs, tbs) = rows[str(1)]
    tbs[0] = blue
    rows[1] = (dbs, tbs)

    for r in rows.keys():
        drawit(r, rows[r])

    for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()

    pygame.display.update()
    fpsClock.tick(30)